��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  1  �  !   /  &   Q  %   x  &   �  z   �  y   @  2   �  4   �  2   "	     U	  (   q	     �	  ]   �	     
     
  l   1
     �
  &   �
     �
  F   �
  K   D     �  "   �     �     �     �                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-20 16:22+0300
Last-Translator: Andrei Stepanov, 2022
Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 3.0.1
 Ускорение (повтор) Установка всех опций Расположение кнопок Сменить тему курсора Не удалось отключить. 
 Пожалуйста, измените ~/.desktop-session/startup вручную Не удалось включить.  
 Пожалуйста, измените ~/.desktop-session/startup вручную Не удалось запустить ds-mouse -a Не удалось запустить ds-mouse -all Не удалось запустить ds-mouse -s Размер курсора Сброс размера курсора Тема курсора Включить или отключить настройки мыши при запуске
 Ошибка Для левшей Возможно потребуется выйти/войти,
чтобы увидеть изменения. Ускорение мыши Сброс ускорения мыши Настройки мыши Настройки мыши загрузятся при запуске Настройки мыши не загрузятся при запуске Для правшей Размер (в пикселях) Запуск Успешно Порог (пикселей) 