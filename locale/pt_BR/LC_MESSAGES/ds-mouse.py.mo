��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �  !   �            a   1  ^   �  1   �  3   $  1   X     �     �     �  H   �     	     	  X   4	     �	  &   �	     �	  A   �	  F   (
     o
     �
     �
     �
     �
                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:38+0300
Last-Translator: marcelo cripe <marcelocripe@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.0.1
 Aceleração (Multiplicador) Todas as Opções Foram Definidas Ordem dos Botões Alterar o tema do cursor Não foi possível desativar. 
Por favor, edite manualmente o arquivo: ~/.desktop-session/startup Não foi possível ativar. 
Por favor, edite manualmente o arquivo: ~/.desktop-session/startup Não foi possível executar o comando ds-mouse -a Não foi possível executar o comando ds-mouse -all Não foi possível executar o comando ds-mouse -s Tamanho do Cursor Redefinir o Tamanho do Cursor Tema do Cursor Ativar ou desativar as configurações do rato/mouse na inicialização
 Erro Esquema para a mão esquerda Pode ser necessário sair e 
entrar na sessão para que 
as alterações sejam ativadas. Aceleração do Rato/Mouse Redefinir a Aceleração do Rato/Mouse Opções do Rato/Mouse A configuração do rato/mouse será carregada na inicialização A configuração do rato/mouse não será carregada na inicialização Esquema para a mão direita Tamanho (em pixels) Inicializar Sucesso Limite (Pixel) 