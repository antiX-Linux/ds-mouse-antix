��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �     �  U   �  [   J  &   �  (   �  &   �       $   2     W  1   j     �     �  A   �     �      		     *	  8   ?	  ;   x	     �	     �	  	   �	     �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2023-01-10 00:57+0200
Last-Translator: Feri, 2022
Language-Team: Hungarian (http://www.transifex.com/anticapitalista/antix-development/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Gyorsulás (szorzó) Minden opció beállítva Gombok sorrendje Téma megváltoztatása Nem sikerült a letiltás.
Szerkessze manuálisan a ~/.desktop-session/startup fájlt Nem sikerült az engedélyezés.
Szerkessze manuálisan a ~/.desktop-session/startup fájlt Nem sikerült a ds-mouse -a futtatása Nem sikerült a ds-mouse -all futtatása Nem sikerült a ds-mouse -s futtatása Egérmutató mérete Egérmutató mérete visszaállítva Egérmutató téma Az egérbeállítások alkalmazása indításkor
 Hiba Balkezes elrendezés Kilépésre és belépésre lehet
szükség a változtatásokhoz. Egér gyorsulás Egér gyorsulás visszaállítva Egér beállítások Az egér beállítások be fognak töltődni induláskor Az egér beállítások nem fognak betöltődni induláskor Jobbkezes elrendezés Méret (képpontban) Indítás Sikeres Küszöbszint (képpont) 