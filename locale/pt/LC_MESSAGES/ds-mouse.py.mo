��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �        O     L   i  1   �  3   �  1        N      `     �  6   �     �     �  C   �     #	  !   0	     R	  4   d	  ?   �	     �	     �	     �	     
     
                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:38+0300
Last-Translator: José Vieira <jvieira33@sapo.pt>
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Aceleração (multiplicador) Todas as opções definidas Esquema dos Botões Alterar o tema do cursor Não consegui desativar.
Por favor edite manualmente ~/.desktop-session/startup Não consegui ativar.
Por favor edite manualmente ~/.desktop-session/startup Não foi possível executar o comando ds-mouse -a Não foi possível executar o comando ds-mouse -all Não foi possível executar o comando ds-mouse -s Tamanho do Cursor Restabelecer o tamanho do cursor Tema do Cursor Ativar ou desativar configuração do rato ao iniciar
 Erro Para mão esquerda Poderá ser necessário fazer logout/lgin
para ver as alterações. Aceleração Restabelecer Aceleração do Rato Opções do rato  A configuração do Rato será carregada ao iniciar. A configuração do Rato não será carregada durante o início Para mão direita Tamanho (píxeis) Início Êxito Limite (pixel) 