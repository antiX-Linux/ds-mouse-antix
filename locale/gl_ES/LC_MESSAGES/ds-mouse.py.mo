��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �          "  E   ;  B   �  .   �  0   �  $   $     I     [     {  7   �     �     �  B   �  
   	      '	     H	  -   Y	  3   �	     �	     �	     �	  
   �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-20 16:21+0300
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020-2021
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
Language: gl_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Velocidade (multiplicador) Todas as opcións establecidas Orde dos botóns Cambiar o tema do cursor Non se puido desactivar.
Edita ~/.desktop-session/startup manualmente Non se puido activar.
Edita ~/.desktop-session/startup manualmente Non foi posible executar o comando ds-mouse -a Non foi posible executar o comando ds-mouse -all Non foi posible executar ds-mouse -s Tamaño do cursor Restablecer o tamaño do cursor Tema do cursor Activa ou desactiva a configuración do rato ao inicio
 Erro Para man esquerda Pode esixirlle pechar sesión/iniciar sesión
para ver os cambios. Velocidade Restablecer a velocidade do rato Opcións do rato A configuración do rato cargarase ao iniciar A configuración do rato non se cargará ao iniciar Para man dereita Tamaño (píxeles) Inicio Con éxito Límite (Píxeles) 