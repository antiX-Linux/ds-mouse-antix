��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �     �  �     ~   �  0     2   N  0   �     �     �     �  .   �     	     $	  t   5	     �	  !   �	     �	  ;   �	  A   (
     j
     |
     �
  "   �
     �
                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:32+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Beschleunigungsfaktor Alle Einstellungen sind gesetzt Tastenordnung Ändern des Mauszeigerdesigns Deaktivieren nicht möglich. 
Bitte nehmen Sie die erforderlichen Einträge in der
Datei ~/.desktop-session/startup manuell vor. Einrichten nicht möglich. 
Bitte nehmen Sie die erforderlichen Einträge in
der Datei ~/.desktop-session/startup manuell vor. »ds-mouse -a« konnte nicht ausgeführt werden. »ds-mouse -all« konnte nicht ausgeführt werden. »ds-mouse -s« konnte nicht ausgeführt werden. Zeigergröße Zeigergröße zurückgesetzt Mauszeigerdesign Laden der Mauskonfiguration beim Startvorgang
 Fehler Linkshändermaus Es ist evtl. erforderlich, sich vom System
ab- und neu anzumelden, damit die
Änderungen des Designs wirksam werden. Mausbeschleunigung Mausbeschleunigung zurückgesetzt Mausoptionen Beim Systemstart wird die Konfiguration der Maus aktiviert. Beim Systemstart wird die Konfiguration der Maus nicht verwendet. Rechtshändermaus Größe (in Pixeln) Systemstart Die Anpassungen wurden vorgenommen Schwellenwert (in Pixeln) 