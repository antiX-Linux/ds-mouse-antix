��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �     �  E     B   [  $   �  &   �  $   �          (     E  ;   X     �     �  ;   �     �  #   	     2	  4   E	  8   z	     �	     �	     �	     �	      
                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:36+0300
Last-Translator: James Bowlin <BitJam@gmail.com>
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Accelerazione (esponenziale) Tutte le opzioni settate Ordinamento dei tasti Cambia tema del puntatore Non posso disabilitare. 
Edita ~/.desktop-session/startup manualmente Non posso abilitare. 
Edita ~/.desktop-session/startup manualmente Non è possibile avviare ds-mouse -a Non è possibile avviare ds-mouse -all Non è possibile avviare ds-mouse -s Dimensione del puntatore Reimposta Dimensione Cursore Tema del puntatore Abilita o disabilita la configurazione del mouse all'avvio
 Errore Impostazione per mano sinistra Potrebbe richiedere logout/login 
per vedere i cambiamenti. Accelerazione del mouse Reimposta l'accelerazione del Mouse Opzioni del mouse  La configurazione del mouse sarà caricata all'avvio La configurazione del mouse non sarà caricata all'avvio Impostazione per mano destra Dimensione (in pixel) Avvio Operazione riuscita Limite (in pixel) 