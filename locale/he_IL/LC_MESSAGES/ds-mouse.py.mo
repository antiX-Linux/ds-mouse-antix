��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  #  �     !  &   7     ^  '   v  [   �  [   �  )   V  +   �  )   �     �     �  #   	  U   '	  
   }	     �	  I   �	     �	     
     !
  6   =
  F   t
     �
     �
  
   �
  
   �
                                                                                	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:36+0300
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>
Language-Team: Hebrew (Israel) (http://www.transifex.com/anticapitalista/antix-development/language/he_IL/)
Language: he_IL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
X-Generator: Poedit 3.0.1
 האצה (מכפיל) כל האפשרויות מוגדרות סדר הכפתורים החלפת ערכת עיצוב הסמן לא ניתן להשבית. 
 נא לערוך את ‎~/.desktop-session/startup ידנית לא ניתן להפעיל. 
 נא לערוך את ‎~/.desktop-session/startup ידנית לא ניתן להריץ את ds-mouse -a לא ניתן להריץ את ds-mouse -all לא ניתן להריץ את ds-mouse -s גודל הסמן איפוס גודל סמן ערכת העיצוב של הסמן הפעלה או השבתה של הגדרות העכבר עם עליית המערכת
 שגיאה פריסה לשמאליים עשוי לדרוש יציאה/כניסה 
לצפייה בשינויים. האצת העכבר איפוס האצת העכבר אפשרויות העכבר הגדרות העכבר תיטענה עם ההפעלה הגדרות העכבר לא תיטענה עם עליית המערכת פריסה לימניים גודל (בפיקסלים) התחלה הצלחה סף (פיקסלים) 