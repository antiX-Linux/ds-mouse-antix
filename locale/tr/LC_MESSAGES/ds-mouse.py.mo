��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �     �  W     M   k  !   �  %   �  !        #     2     O  U   _     �     �  C   �     	     	     5	  2   G	  5   z	     �	     �	     �	     �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:38+0300
Last-Translator: mahmut özcan <faradundamarti@yandex.com>
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.0.1
 İvme (Artırıcı) Tüm Seçenekler Ayarı Düğme Sırası İmleç temasını değiştir Devre dışı bırakılamadı.
 Lütfen ~/.desktop-session/startup'ı elle düzenleyin  Etkinleştirilemedi.
 Lütfen ~/.desktop-session/startup'ı elle düzenleyin  ds-mouse çalıştırılamadı -a ds-mouse çalıştırılamadı -hepsi ds-mouse çalıştırılamadı -s İmleç Boyutu İmleç Boyutunu Sıfırlama İmleç Teması Başlangıçta fare yapılandırmasını etkinleştirin veya devre dışı bırakın
 Hata Sol el düzeni Değişiklikleri görmek için
oturumu kapatıp açmak gerekebilir. Fare İvmesi Fare İvmesini Sıfırlama Fare Seçenekleri Fare yapılandırması başlangıçta yüklenecek  Fare yapılandırması başlangıçta yüklenmeyecek  Sağ el düzeni Boyut (piksel olarak) Başlangıç Başarılı Eşik (Piksel) 