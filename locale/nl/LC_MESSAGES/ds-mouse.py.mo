��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �     �  I     H   Q     �      �     �     �            <   ,     i     n  ;   �     �     �     �  0   �  5   %	     [	     p	  	   �	     �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:38+0300
Last-Translator: James Bowlin <BitJam@gmail.com>
Language-Team: Dutch (http://www.transifex.com/anticapitalista/antix-development/language/nl/)
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Acceleratie (Multiplier) Alle Opties Ingesteld Volgorde van knoppen Verander Cursor-Thema Kon niet uitschakelen. 
 Pas aub ~/.desktop-session/startup handmatig aan Kon niet inschakelen. 
 Pas aub ~/.desktop-session/startup handmatig aan Kon ds-mouse -a niet uitvoeren Kon ds-mouse -all niet uitvoeren Kon ds-mouse -s niet uitvoeren Cursor-grootte Cursor Grootte Herstel Cursor-thema Muisconfiguratie tijdens het opstarten aan- of uitschakelen
 Fout Linkshandige layout Eventueel logout/login vereist
om de veranderingen te zien. Muis-acceleratie Muis Acceleratie Herstel Muis-opties Muisconfiguratie zal laden tijdens het opstarten Muisconfiguratie zal niet laden tijdens het opstarten Rechtshandige layout Grootte (in pixels) Opstarten Gelukt Drempelwaarde (Pixels) 