��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �  	     D     E   P  "   �  $   �  "   �            	   .  0   8     i     n  5   �     �     �     �  (   �  -   	     M	     ^	     u	  	   ~	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:37+0300
Last-Translator: heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Akselerasjon (multiplikator) Alle alternativer valgt Knapperekkefølge Musepeker Klarte ikke slå av.
Rediger fila ~/.desktop-session/startup manuelt Klarte ikke slå på.
Rediger fila ~/.desktop-session/startup manuelt Klarte ikke kjøre «ds-mouse -a» Klarte ikke kjøre «ds-mouse -all» Klarte ikke kjøre «ds-mouse -s» Pekerstørrelse Tilbakestill pekerstørrelse Pekertema Slå av eller på oppsett av mus under oppstart
 Feil Venstrehåndsbruk Kan kreve inn- eller utlogging 
for å se endringene. Museakselerasjon Tilbakestill museakselerasjon Musealternativer Oppsett av mus vil lastes under oppstart Oppsett av mus vil ikke lastes under oppstart Høyrehåndsbruk Størrelse (i piksler) Oppstart Vellykket Terskel (piksler) 