��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �       F   ,  C   s     �  !   �     �          ,     L  ?   \     �     �  =   �     �  &   	     7	  1   L	  4   ~	     �	     �	     �	     �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2023-01-10 00:57+0200
Last-Translator: Amigo, 2022
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 3.0.1
 Aceleración (Multiplicador) Todas las opciones ajustadas Orden de los Botones Cambiar el tema del cursor No se pudo deshabilitar.
 Edite ~/.desktop-session/startup manualmente No se pudo habilitar.
 Edite ~/.desktop-session/startup manualmente No se pudo ejecutar ds-mouse -a No se pudo ejecutar ds-mouse -all No se pudo ejecutar ds-mouse -s Tamaño del Cursor Reajuste del tamaño del cursor Tema del Cursor Habilitar o Deshabilitar la configuración del mouse al inicio
 Error Disposición para zurdos Puede requerir cerrar/iniciar sesión 
para ver los cambios.  Aceleración del Mouse Reajuste de la aceleración del Ratón Opciones del ratón  La configuración del mouse se cargará al inicio La configuración del mouse no se cargará al inicio Disposición para diestros Tamaño (en pixeles) Inicio Éxito Umbral (Pixeles) 