��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �  (   �  $   �     �     �  �     �   �  $   p  &   �  $   �     �  $   �     	  =   2	  	   p	     z	  d   �	  '   �	  6   #
     Z
  9   s
  9   �
     �
          "     )     0                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:37+0300
Last-Translator: Transifex Bot <>
Language-Team: Japanese (http://www.transifex.com/anticapitalista/antix-development/language/ja/)
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 3.0.1
 アクセラレーション (Multiplier) 全てのオプションをセット ボタンの順番 カーソルテーマの変更 起動時に無効にすることができせんでした。
ユーザー・ディレクトリの desktop-session/startup ファイルを手動で編集してください 起動時に有効にすることができせんでした。
ユーザー・ディレクトリの desktop-session/startup ファイルを手動で編集してください ds-mouse -a を開始できません ds-mouse -all を開始できません ds-mouse -s を開始できません カーソルサイズ カーソルサイズのリセット カーソルテーマ 起動時にマウスの設定を有効／無効にします
 エラー 左利き用レイアウト 変更を確認するには、ログアウト/ログインが必要となる場合があります。 マウスのアクセラレーション マウスのアクセラレーションのリセット マウスオプション マウスの設定は起動時に読み込まれません マウスの設定は起動時に読み込まれません 右利き用レイアウト サイズ (ピクセル表示) 起動 成功 しきい値 (ピクセル) 