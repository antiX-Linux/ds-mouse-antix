��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �     �  O     N   `  !   �     �     �                =  8   K     �     �  ;   �     �      �     	  (   	  -   H	     v	     �	     �	     �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:38+0300
Last-Translator: Henry Oquist <henryoquist@nomalm.se>
Language-Team: Swedish (http://www.transifex.com/anticapitalista/antix-development/language/sv/)
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Acceleration (Multiplikator) Alla Valmöjligheter Inställda Knappordning Byt muspekar-tema Kunde inte avaktivera.
Var vänlig redigera ~/.desktop-session/startup manuellt Kunde inte aktivera. 
Var vänlig redigera ~/.desktop-session/startup manuellt Kunde inte köra ds-mouse -all -a Kunde inte köra ds-mouse -all Kunde inte köra ds-mouse -s Markörstorlek Muspekar-storlek Återställning Muspekar-tema Aktivera eller Avaktivera mus-konfiguration med startup
 Fel Vänsterhänt layout Kan behöva utloggning/inloggning
för att se ändringarna. Mus-acceleration Mus-acceleration Återställning Mus Valmöjligheter Mus-konfiguration sätts på med startup Mus-konfiguration sätts inte på med startup Högerhänt layout Storlek (i pixels) Startup Det lyckades Tröskel (Pixels) 