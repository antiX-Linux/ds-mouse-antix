��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �       6   !  @   X     �     �     �     �     �       9   "     \     b  F   z     �     �     �  .   �  0   ,	     ]	     s	     �	     �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:34+0300
Last-Translator: Oi Suomi On! <oisuomion@protonmail.com>
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Kiihdytys (moninkertaistus) Kaikki vaihtoehdot asetettu  Painikkeiden järjestys Muuta kohdistimen teemaa Ei voitu poistaa. 
 Muokkaa ~/.desktop-session/startup Ei voitu ottaa käyttöön. 
 Muokkaa ~/.desktop-session/startup ds-mouse -a ei voitu ajaa ds-mouse -all ei voitu ajaa ds-mouse -s ei voitu ajaa Osoittimen koko Palauta osoittimen koko Osoittimen teema Käytä tai poista hiiren määritys käynnistettäessä
 Virhe Vasemmankäden asettelu Saattaa vaatia kirjautumisen ulos/sisään 
jotta muutokset näkyvät. Hiiren kiihtyvyys Nollaa hiiren kiihtyvyys Hiiren asetukset Hiiren määritys latautuu käynnistettäessä Hiiren määritys ei lataudu käynnistettäessä Oikeankäden asettelu Koko (pikseleinä) Käynnistä Onnistui Kynnysarvo (pikselit) 