��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �               -  ?   E  >   �  #   �  %   �  #        2     C     b  /   q     �     �  9   �     �     
	     (	  0   <	  3   m	     �	     �	     �	     �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:38+0300
Last-Translator: Arnold Marko <arnold.marko@gmail.com>
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 3.0.1
 Pospešek (večkratnik) Nastavljene so vse opciije Zaporedje tipk Zamanjaj temo za kurzor Izklop ni bil mogoč.
Ročno uredite ~/.desktop-session/startup Vklop ni bil mogoč.
Ročno uredite ~/.desktop-session/startup Ni bilo mogoče zagnati ds-mouse -a Ni bilo mogoče zagnati ds-mouse -all Ni bilo mogoče zagnati ds-mouse -s Velikost kazalca Ponastavitev velikosti kazalca Tema za kurzor Vklopi ali izklopi nastavitve miške ob zagonu
 Napaka Razpored za levičarje Da bodo spremembe vidne,
bo morda potreben ponovni zagon. Pospešek miške Ponastavitev pospeška miške Možnosti za miško Nastavitve za miško se bodo naložile ob zagonu Nastavitve za miško se ob zagonu ne bodo naložile Razpored za desničarje Velikost (v pikslih) Zagon Uspešno Prag (pikslov) 