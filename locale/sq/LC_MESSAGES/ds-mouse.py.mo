��          �   %   �      `     a     {     �     �  D   �  C   �     5     O     k     �     �     �  1   �     �     �  -   �     '     :     S  (   a  ,   �     �     �     �     �     �  �  �     �     �     �       O     P   l  "   �  $   �  "        (     9     T  9   b     �     �  6   �     �     	     	  1   ,	  5   ^	     �	     �	     �	     �	     �	                                                                           	                      
                           Acceleration (Multiplier) All Options Set Button Order Change cursor theme Could not disable. 
 Please edit ~/.desktop-session/startup manually Could not enable. 
 Please edit ~/.desktop-session/startup manually Could not run ds-mouse -a Could not run ds-mouse -all Could not run ds-mouse -s Cursor Size Cursor Size Reset Cursor Theme Enable or Disable mouse configuration on startup
 Error Left hand layout May require logout/login 
to see the changes. Mouse Acceleration Mouse Acceleration Reset Mouse Options Mouse configuration will load on startup Mouse configuration will not load on startup Right hand layout Size (in pixels) Startup Success Threshold (Pixels) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2022-06-18 19:38+0300
Last-Translator: Besnik Bleta <besnik@programeshqip.org>
Language-Team: Albanian (http://www.transifex.com/anticapitalista/antix-development/language/sq/)
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Përshpejtim (Shumëfishues) Krejt Mundësitë të Ujdisura Radhë Butonash Ndryshoni temë kursori S’u aktivizua dot.
 Ju lutemi, përpunojeni ~/.desktop-session/startup dorazi S’u aktivizua dot. 
 Ju lutemi, përpunojeni ~/.desktop-session/startup dorazi S’u xhirua dot “ds-mouse -a” S’u xhirua dot “ds-mouse -all” S’u xhirua dot “ds-mouse -s” Madhësi Kursori Ricaktim Madhësie Kursori Temë Kursori Aktivizoni ose Çaktivizoni formësim miu gjatë nisjesh
 Gabim Skemë për mëngjarashë Mund të lypë dalje/hyrje 
që të shihni ndryshimet. Përshpejtim Miu Ricaktim Përshpejtimi Miu Mundësi Miu Formësimi i miut do të ngarkohet gjatë nisjesh Formësimi i miut s’do të ngarkohet gjatë nisjesh Skemë për djathtakë Madhësi (në piksel) Nisje Sukses Prag (Piksel) 